## Discontinuous Galerkin method for 1D Euler equations

Implementation of the Discontinuous Galerkin method for 1D Euler equations with C++. 
For the basis functions I used Legendre polynomials and for the numerical flux at the 
cell boundaries I used the Van Leer flux. The Sod shock tube problem serves as a test case.
Code is parallelized with Open MPI. Further documentation can be found [here](https://gitlab.com/tobiasjaeger/eulerequations1d/blob/master/README.pdf).