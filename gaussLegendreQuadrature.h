/*
   modified code from http://rosettacode.org/wiki/Numerical_integration/Gauss-Legendre_Quadrature
   to calculate the weights and roots for the gauss legendre quadrature
 */
template <typename precision, int eDEGREE>
class gaussLegendreQuadrature
{
public:
gaussLegendreQuadrature()
{
        // Solve roots and weights
        for (int i = 0; i <= eDEGREE; ++i)
        {
                precision dr = 1;

                // Find zero
                Evaluation eval(cos(M_PI * (i - 0.25) / (eDEGREE + 0.5)));
                do
                {
                        dr = eval.v() / eval.d();
                        eval.evaluate(eval.x() - dr);
                } while (fabs(dr) > 2e-16);

                this->_r[i] = eval.x();
                this->_w[i] = 2 / ((1 - eval.x() * eval.x()) * eval.d() * eval.d());
        }
}

precision root(int i) const {
        return this->_r[i];
}
precision weight(int i) const {
        return this->_w[i];
}

void getRoots(precision roots[])
{
        for (int i = 1; i <= eDEGREE; ++i)
        {
                roots[i - 1] = this->_r[i];
        }
}

void getWeights(precision weights[])
{
        for (int i = 1; i <= eDEGREE; ++i)
        {
                weights[i - 1] = this->_w[i];
        }
}

private:
precision _r[eDEGREE + 1];
precision _w[eDEGREE + 1];

/*! Evaluate the value *and* derivative of the
 *   Legendre polynomial
 */
class Evaluation
{
public:
explicit Evaluation(precision x) : _x(x), _v(1), _d(0)
{
        this->evaluate(x);
}

void evaluate(precision x)
{
        this->_x = x;

        precision vsub1 = x;
        precision vsub2 = 1;
        precision f = 1 / (x * x - 1);

        for (int i = 2; i <= eDEGREE; ++i)
        {
                this->_v = ((2 * i - 1) * x * vsub1 - (i - 1) * vsub2) / i;
                this->_d = i * f * (x * this->_v - vsub1);

                vsub2 = vsub1;
                vsub1 = this->_v;
        }
}

precision v() const {
        return this->_v;
}
precision d() const {
        return this->_d;
}
precision x() const {
        return this->_x;
}

private:
precision _x;
precision _v;
precision _d;
};
};
