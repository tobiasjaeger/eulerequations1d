template <typename precision, int degree>
class legendrePolynomials
{
private:

static const int numberGaussLegendrePoints = degree + 1;
static const int numberLegendrePoly = 5;
precision normLegendre[numberLegendrePoly];

precision legendreEvaluations[degree][numberGaussLegendrePoints + 2];
precision derivativeLegendreEvaluations[degree][numberGaussLegendrePoints];

typedef precision (legendrePolynomials::*function1)(precision z);
function1 p[numberLegendrePoly];
function1 d[numberLegendrePoly];

public:
legendrePolynomials();

void evaluate(precision gaussLegendreRoots[]);

precision getLegendreEvaluations(int i, int j);
precision getDerivativeLegendreEvaluations(int i, int j);

precision legendrePoly1(precision z);
precision legendrePoly2(precision z);
precision legendrePoly3(precision z);
precision legendrePoly4(precision z);
precision legendrePoly5(precision z);
precision legendrePoly(int n, precision z);

precision derivativeLegendrePoly1(precision z);
precision derivativeLegendrePoly2(precision z);
precision derivativeLegendrePoly3(precision z);
precision derivativeLegendrePoly4(precision z);
precision derivativeLegendrePoly5(precision z);
precision derivativeLegendrePoly(int n, precision z);


};

template <typename precision, int degree>
legendrePolynomials<precision, degree>::legendrePolynomials(){
        normLegendre[0] = sqrt(3.0);
        normLegendre[1] = sqrt(5.0) / 2.0;
        normLegendre[2] = sqrt(7.0) / 2.0;
        normLegendre[3] = sqrt(9.0) / 8.0;
        normLegendre[4] = sqrt(11.0) / 8.0;

        p[0] =  &legendrePolynomials::legendrePoly1;
        p[1] =  &legendrePolynomials::legendrePoly2;
        p[2] =  &legendrePolynomials::legendrePoly3;
        p[3] =  &legendrePolynomials::legendrePoly4;
        p[4] =  &legendrePolynomials::legendrePoly5;

        d[0] =  &legendrePolynomials::derivativeLegendrePoly1;
        d[1] =  &legendrePolynomials::derivativeLegendrePoly2;
        d[2] =  &legendrePolynomials::derivativeLegendrePoly3;
        d[3] =  &legendrePolynomials::derivativeLegendrePoly4;
        d[4] =  &legendrePolynomials::derivativeLegendrePoly5;

}

template <typename precision, int degree>
void legendrePolynomials<precision, degree>::evaluate(precision gaussLegendreRoots[])
{
        for (int i = 0; i < degree; i++) {
                legendreEvaluations[i][0] = legendrePoly(i, -1.0);
                legendreEvaluations[i][1] = legendrePoly(i, 1.0);
                for (int j = 0; j < numberGaussLegendrePoints; j++) {
                        legendreEvaluations[i][j+2] = legendrePoly(i, gaussLegendreRoots[j]);
                        derivativeLegendreEvaluations[i][j] = derivativeLegendrePoly(i, gaussLegendreRoots[j]);
                }
        }
}

template <typename precision, int degree>
precision legendrePolynomials<precision, degree>::legendrePoly1(precision z)
{
        return normLegendre[0] * z;
}

template <typename precision, int degree>
precision legendrePolynomials<precision, degree>::legendrePoly2(precision z)
{
        return normLegendre[1] * (3.0 * z * z - 1.0);
}

template <typename precision, int degree>
precision legendrePolynomials<precision, degree>::legendrePoly3(precision z)
{
        return normLegendre[2] * (5.0 * z * z * z - 3.0 * z);
}

template <typename precision, int degree>
precision legendrePolynomials<precision, degree>::legendrePoly4(precision z)
{
        return normLegendre[3] * (35.0 * z * z * z * z - 30.0 * z * z + 3.0);
}

template <typename precision, int degree>
precision legendrePolynomials<precision, degree>::legendrePoly5(precision z)
{
        return normLegendre[4] * (63.0 * z * z * z * z * z - 70.0 * z * z * z + 15.0 * z);
}

template <typename precision, int degree>
precision legendrePolynomials<precision, degree>::legendrePoly(int n, precision z)
{
        return (this->*(p[n]))(z);
}

template <typename precision, int degree>
precision legendrePolynomials<precision, degree>::derivativeLegendrePoly1(precision z)
{
        return normLegendre[0];
}

template <typename precision, int degree>
precision legendrePolynomials<precision, degree>::derivativeLegendrePoly2(precision z)
{
        return normLegendre[1] * 6.0 * z;
}

template <typename precision, int degree>
precision legendrePolynomials<precision, degree>::derivativeLegendrePoly3(precision z)
{
        return normLegendre[2] * (15.0 * z * z - 3.0);
}

template <typename precision, int degree>
precision legendrePolynomials<precision, degree>::derivativeLegendrePoly4(precision z)
{
        return normLegendre[3] * (140.0 * z * z * z - 60.0 * z);
}

template <typename precision, int degree>
precision legendrePolynomials<precision, degree>::derivativeLegendrePoly5(precision z)
{
        return normLegendre[4] * (315.0 * z * z * z * z - 210.0 * z * z + 15.0);
}

template <typename precision, int degree>
precision legendrePolynomials<precision, degree>::derivativeLegendrePoly(int n, precision z)
{
        return (this->*(d[n]))(z);
}

template <typename precision, int degree>
precision legendrePolynomials<precision, degree>::getLegendreEvaluations(int i, int j)
{
        return legendreEvaluations[i][j];
}

template <typename precision, int degree>
precision legendrePolynomials<precision, degree>::getDerivativeLegendreEvaluations(int i, int j)
{
        return derivativeLegendreEvaluations[i][j];
}
