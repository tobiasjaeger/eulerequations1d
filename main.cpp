/*
   Solve the 1D euler equations with the Discontinuous Galerkin method.
   The Van Leer flux is used as numerical fluxes between the cells. Slope limiter
   for higher order solvers is still missing.
   The solver is parallelised by using OpenMPI.
   As a test case the solver is applied to the Sod shock tube problem and to
   a smooth pressure pertubation.

   Tobias Jäger December, 2018
 */

#include <iostream>
#include <fstream>
#include <vector>
#include <array>
#include <math.h>

#include "mpi.h"

#include "gaussLegendreQuadrature.h"
#include "cell.h"
#include "legendrePolynomials.h"
#include "grid.h"

double pressure(double x){
        double xc = x - 0.5;
        double sig = 0.05;
        return 0.1 * exp(-0.5 * xc * xc / sig / sig) + 1.0;
}

double velocity(double x){
        return 0.0;
}

double density(double x){
        return 1.0;
}

int main(int argc, char *argv[])
{
        double t = 1.5;
        double dt = 1.0e-2;
        int numberCells = 90;
        const int degreePoly = 0;
        int numberSteps = ceil(t/dt);
        //int numberSteps = 1;
        double gridLength = 10.0;
        double gamma = 1.4;

        grid<double, degreePoly> globalGrid(gridLength, dt, gamma);

        // smooth pressure pertubation
        /*
           globalGrid.init(numberCells, density, velocity, pressure);
           globalGrid.saveData("init0", degreePoly + 1);
           globalGrid.runFem(numberSteps);
           globalGrid.saveData("end0", degreePoly + 1);
         */
        // Sod shock tube
        globalGrid.runSodShockTubeUniformGridMpi(numberCells, numberSteps, argc, argv);
        /*
           globalGrid.initSodShockTubeUniformGrid(numberCells, 1.0, 0.0, 1.0, 0.25, 0.0, 0.2);
           globalGrid.saveData("initSod", degreePoly + 1);
           globalGrid.runFem(numberSteps);
           globalGrid.saveData("endSod", degreePoly + 1);
         */
        return 0;
}
