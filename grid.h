/*
   Implementation of the class grid which contains all cells and the methods to
   calculate the numerical flux (Van Leer flux) and to update the cells. For the time integration
   total variation diminishing Runge Kutta methods are used. The order of the
   Runge Kutta method matches the spatial order of the Discontinuous Galerkin method.
   Up to now only polynomials up to degree 2 are supported.
 */

template <typename precision, int degree>
class grid
{
private:
precision length;
precision dt;
precision gamma;
precision gamma1;
precision gamma2;
precision gamma3;
static const int degreePoly = degree;
std::vector<std::array<precision, 3> > numericalFluxes;
std::vector<cell<precision, degree> > cells;

legendrePolynomials<precision, degree> lp;

static const int numberGaussLegendrePoints = degree + 1;
precision gaussLegendreWeights[numberGaussLegendrePoints];
precision gaussLegendreRoots[numberGaussLegendrePoints];

typedef void (grid::*function2)(int cellIndex); //pointer to function this is slow and should be replace by functors
function2 rk1[1];
function2 rk2[2];
function2 rk3[3];
typedef void (grid::*function3)(int n, int cellIndex); //pointer to function this is slow and should be replace by functors
function3 rk[3];

public:
grid(precision _length, precision _dt, precision _gamma);

void initSodShockTubeUniformGrid(int numberCells, precision massDensityLeft,
                                 precision velocityLeft, precision pressureLeft,
                                 precision massDensityRight,
                                 precision velocityRight,
                                 precision pressureRight);

void init(int numberCells, precision (*density)(precision),
          precision (*velocity)(precision), precision (*pressure)(precision));

void copyAllWeights(precision allWeights[]);
void updateAllWeights(precision newWeights[]);

void updateCellFvm(int cellIndex);

void updateBoundaryCellLeft();
void updateBoundaryCellRight();

void updateCellsFvmVanLeer(int cellIndex1, int cellIndex2);

precision calculateCflNumber(int cellIndex, precision z);

void updateCflNumber(int cellIndex);

int getNumberCells();

void calculateNumericalFluxLuxFriedrichs(int cellIndex1, int cellIndex2);

void calculateNumericalFluxVanLeerFvmOpt(int cellIndex1, int cellIndex2);

void calculateNumericalFluxVanLeer(int cellIndex1, int cellIndex2);

void addCell(precision _length, precision _position,
             precision _massDensityWeights[],
             precision _momentumDensityWeights[],
             precision _energyDensityWeights[], int _boundary);

int getDegreePoly();

precision getLength();

precision getCellLength(int cellIndex);

precision getMassDensity(int cellIndex, precision z);

void getMassDensityWeights(int cellIndex, precision massDensityWeights[]);

void getMomentumDensityWeights(int cellIndex, precision momentumDensityWeights[]);

void getEnergyDensityWeights(int cellIndex, precision energyDensityWeights[]);

precision getVelocity(int cellIndex, precision z);

precision getMomentumDensity(int cellIndex, precision z);

precision getPressure(int cellIndex, precision z);

precision calculatePressure(precision massDensity, precision momentumDensity,
                            precision energyDensity);

precision getEnergyDensity(int cellIndex, precision z);

precision getSoundSpeed(int cellIndex, precision z);

precision getMachNumber(int cellIndex, precision z);

precision calculateSoundSpeed(precision massDensity, precision pressure);

precision getBoundaryMassDensity(int cellIndex, int b);

precision getBoundaryMomentumDensity(int cellIndex, int b);

precision getBoundaryEnergyDensity(int cellIndex, int b);

precision evaluateMassDensity(int cellIndex, int i);

precision evaluateMomentumDensity(int cellIndex, int i);

precision evaluateEnergyDensity(int cellIndex, int i);

void calculateDerivativeWeights(int cellIndex, int n);

void updateCellsFem();

void updateCellsFemMpi(int numprocs, int procid);

void updateCellsFvm(int cellIndex1, int cellIndex2);

void rungeKuttaMethod(int k, int n, int cellIndex);

void rungeKuttaMethod3(int n, int cellIndex);
void rungeKuttaMethod2(int n, int cellIndex);
void rungeKuttaMethod1(int n, int cellIndex);

void rungeKuttaMethod3Step1(int cellIndex);
void rungeKuttaMethod3Step2(int cellIndex);
void rungeKuttaMethod3Step3(int cellIndex);

void rungeKuttaMethod2Step1(int cellIndex);
void rungeKuttaMethod2Step2(int cellIndex);

void rungeKuttaMethod1Step1(int cellIndex);

precision getWeight(int cellIndex, int i);

void runFem(int numberSteps);

void exchangeDataMpi(int numprocs, int procid);

void runSodShockTubeUniformGridMpi(int numberCells, int numberSteps,
                                   int argc, char *argv[]);

void saveData(std::string name, int n);

};

template <typename precision, int degree>
grid<precision, degree>::grid(precision _length, precision _dt, precision _gamma)
{
        dt = _dt;
        length = _length;
        gamma = _gamma;
        gamma1 = gamma - 1.0;
        gamma2 = gamma * gamma - 1.0;
        gamma3 = gamma - 3.0;

        std::array<precision, 3> numericalFlux;
        numericalFluxes.push_back(numericalFlux);

        rk1[0] = &grid::rungeKuttaMethod1Step1;

        rk2[0] = &grid::rungeKuttaMethod2Step1;
        rk2[1] = &grid::rungeKuttaMethod2Step2;

        rk3[0] = &grid::rungeKuttaMethod3Step1;
        rk3[1] = &grid::rungeKuttaMethod3Step2;
        rk3[2] = &grid::rungeKuttaMethod3Step3;

        rk[0] = &grid::rungeKuttaMethod1;
        rk[1] = &grid::rungeKuttaMethod2;
        rk[2] = &grid::rungeKuttaMethod3;

        if (degree > 0)
        {
                gaussLegendreQuadrature<precision, numberGaussLegendrePoints> gaussLegendre;
                gaussLegendre.getWeights(gaussLegendreWeights);
                gaussLegendre.getRoots(gaussLegendreRoots);
                lp.evaluate(gaussLegendreRoots);
        }

}

template <typename precision, int degree>
void grid<precision, degree>::initSodShockTubeUniformGrid(int numberCells,
                                                          precision massDensityLeft,
                                                          precision velocityLeft,
                                                          precision pressureLeft,
                                                          precision massDensityRight,
                                                          precision velocityRight,
                                                          precision pressureRight)
{
        precision massDensityWeights[degreePoly + 1];
        precision momentumDensityWeights[degreePoly + 1];
        precision energyDensityWeights[degreePoly + 1];

        precision cellLength = getLength() / numberCells;

        precision momentumDensityLeft = massDensityLeft * velocityLeft;
        precision energyDensityLeft = pressureLeft / gamma1 + velocityLeft
                                      * momentumDensityLeft / 2.0;

        precision momentumDensityRight = massDensityRight * velocityRight;
        precision energyDensityRight = pressureRight / gamma1 + velocityRight
                                       * momentumDensityRight / 2.0;

        for (int i = 0; i != degreePoly; i++)
        {
                massDensityWeights[i + 1] = 0.0;
                momentumDensityWeights[i + 1] = 0.0;
                energyDensityWeights[i + 1] = 0.0;
        }

        massDensityWeights[0] = massDensityLeft;
        momentumDensityWeights[0] = momentumDensityLeft;
        energyDensityWeights[0] = energyDensityLeft;

        addCell(cellLength, 0.0, massDensityWeights, momentumDensityWeights,
                energyDensityWeights, -1);   //boundary cell left

        for (int i = 0; i != numberCells / 2; i++)
        {
                addCell(cellLength, (i + 0.5) * cellLength, massDensityWeights,
                        momentumDensityWeights, energyDensityWeights, 0);
        }

        massDensityWeights[0] = massDensityRight;
        momentumDensityWeights[0] = momentumDensityRight;
        energyDensityWeights[0] = energyDensityRight;

        for (int i = numberCells / 2; i != numberCells; i++)
        {
                addCell(cellLength, (i + 0.5) * cellLength, massDensityWeights,
                        momentumDensityWeights, energyDensityWeights, 0);
        }

        addCell(cellLength, getLength(), massDensityWeights, momentumDensityWeights,
                energyDensityWeights, 1); //boundary cell right
}


template <typename precision, int degree>
void grid<precision, degree>::init(int numberCells, precision (*density)(precision),
                                   precision (*velocity)(precision),
                                   precision (*pressure)(precision)){

        precision massDensityWeights[degreePoly + 1];
        precision momentumDensityWeights[degreePoly + 1];
        precision energyDensityWeights[degreePoly + 1];

        precision cellLength = getLength() / numberCells;
        precision x0, c, d, m, p, v, e;

        precision dx = 1.0 / numberCells;

        for (int i = 0; i != numberCells; i++)
        {
                x0 = dx * (i + 0.5);

                for (int j = 0; j != degreePoly + 1; j++)
                {
                        massDensityWeights[j] = 0.0;
                        momentumDensityWeights[j] = 0.0;
                        energyDensityWeights[j] = 0.0;

                        for (int k = 0; k != numberGaussLegendrePoints; k++)
                        {
                                if (j > 0) {
                                        c = 0.5 * gaussLegendreWeights[k]
                                            * lp.legendrePoly(j - 1, gaussLegendreRoots[k]);

                                        d = density(x0 + gaussLegendreRoots[k] * 0.5 * dx);
                                        v = velocity(x0 + gaussLegendreRoots[k] * 0.5 * dx);
                                        p = pressure(x0 + gaussLegendreRoots[k] * 0.5 * dx);
                                }else{
                                        if (degreePoly > 0) {
                                                c = 0.5 * gaussLegendreWeights[k];
                                                d = density(x0 + gaussLegendreRoots[k] * 0.5 * dx);
                                                v = velocity(x0 + gaussLegendreRoots[k] * 0.5 * dx);
                                                p = pressure(x0 + gaussLegendreRoots[k] * 0.5 * dx);
                                        } else{
                                                c = 1.0;
                                                d = density(x0);
                                                v = velocity(x0);
                                                p = pressure(x0);
                                        }


                                }

                                m = d * v;
                                e = p / gamma1 + v * m / 2.0;

                                massDensityWeights[j] += c * d;
                                momentumDensityWeights[j] += c * m;
                                energyDensityWeights[j] += c * e;
                        }

                }

                if (i == 0) {
                        addCell(cellLength, 0.0, massDensityWeights,
                                momentumDensityWeights, energyDensityWeights, -1);
                }

                addCell(cellLength, (i + 0.5) * cellLength, massDensityWeights,
                        momentumDensityWeights, energyDensityWeights, 0);
        }

        addCell(cellLength, getLength(), massDensityWeights, momentumDensityWeights,
                energyDensityWeights, 1);

}

template <typename precision, int degree>
void grid<precision, degree>::copyAllWeights(precision allWeights[]){
        int n = 3 * (degreePoly + 1);
        for (int i = 0; i != getNumberCells(); i++) {
                for (int j = 0; j != n; j++) {
                        allWeights[i * n + j] = cells[i].getWeight(j);
                }
        }
}

template <typename precision, int degree>
void grid<precision, degree>::updateAllWeights(precision newWeights[]){
        int n = 3 * (degreePoly + 1);
        for (int i = 0; i != getNumberCells(); i++) {
                for (int j = 0; j != 3 * (degreePoly + 1); j++) {
                        cells[i].updateWeight(j, newWeights[i * n + j]);
                }
        }
}

template <typename precision, int degree>
void grid<precision, degree>::updateCellFvm(int cellIndex)
{
        precision l = getCellLength(cellIndex);

        precision newMassDensity;
        precision newMomentumDensity;
        precision newEnergyDensity;

        newMassDensity = cells[cellIndex].getMassDensityWeight(0) - dt / l
                         * (numericalFluxes[cellIndex + 1][0] - numericalFluxes[cellIndex][0]);
        newMomentumDensity = cells[cellIndex].getMomentumDensityWeight(0) - dt / l
                             * (numericalFluxes[cellIndex + 1][1] - numericalFluxes[cellIndex][1]);
        newEnergyDensity = cells[cellIndex].getEnergyDensityWeight(0) - dt / l
                           * (numericalFluxes[cellIndex + 1][2] - numericalFluxes[cellIndex][2]);

        cells[cellIndex].updateWeight(0, newMassDensity);
        cells[cellIndex].updateWeight(degreePoly + 1, newMomentumDensity);
        cells[cellIndex].updateWeight(2 * (degreePoly + 1), newEnergyDensity);
}

template <typename precision, int degree>
void grid<precision, degree>::updateBoundaryCellLeft()
{
        precision newMassDensity[degreePoly + 1];
        precision newMomentumDensity[degreePoly + 1];
        precision newEnergyDensity[degreePoly + 1];

        getMassDensityWeights(1, newMassDensity);
        getMomentumDensityWeights(1, newMomentumDensity);
        getEnergyDensityWeights(1, newEnergyDensity);

        cells[0].updateMassDensity(newMassDensity);
        cells[0].updateMomentumDensity(newMomentumDensity);
        cells[0].updateEnergyDensity(newEnergyDensity);
}

template <typename precision, int degree>
void grid<precision, degree>::updateBoundaryCellRight()
{
        precision newMassDensity[degreePoly + 1];
        precision newMomentumDensity[degreePoly + 1];
        precision newEnergyDensity[degreePoly + 1];

        int numberCells = getNumberCells();

        getMassDensityWeights(numberCells - 2, newMassDensity);
        getMomentumDensityWeights(numberCells - 2, newMomentumDensity);
        getEnergyDensityWeights(numberCells - 2, newEnergyDensity);

        cells[numberCells - 1].updateMassDensity(newMassDensity);
        cells[numberCells - 1].updateMomentumDensity(newMomentumDensity);
        cells[numberCells - 1].updateEnergyDensity(newEnergyDensity);
}

template <typename precision, int degree>
void grid<precision, degree>::updateCellsFvmVanLeer(int cellIndex1, int cellIndex2)
{
        calculateNumericalFluxVanLeerFvmOpt(cellIndex1, cellIndex1 + 1);
        calculateNumericalFluxVanLeerFvmOpt(cellIndex1 + 1, cellIndex1 + 2);

        for (int i = cellIndex1 + 2; i != cellIndex2; i++)
        {
                calculateNumericalFluxVanLeerFvmOpt(i, i + 1);
                updateCellFvm(i - 1);
                updateCflNumber(i - 1);
        }
        updateBoundaryCellLeft();
        updateBoundaryCellRight();
}

template <typename precision, int degree>
precision grid<precision, degree>::calculateCflNumber(int cellIndex, precision z)
{
        return (abs(getVelocity(cellIndex, z)) + getSoundSpeed(cellIndex, z))
               / cells[cellIndex].getLength() * dt * (2 * degreePoly + 1);
}

template <typename precision, int degree>
void grid<precision, degree>::updateCflNumber(int cellIndex)
{
        precision cflNumber = calculateCflNumber(cellIndex, 0.0);
        if (cflNumber > 1.0)
        {
                std::cout << "!!!WARNING!!! in cell " << cellIndex
                          << " CFL number = " << cflNumber << std::endl;
        }
}

template <typename precision, int degree>
int grid<precision, degree>::getNumberCells()
{
        return cells.size();
}

template <typename precision, int degree>
void grid<precision, degree>::calculateNumericalFluxLuxFriedrichs(int cellIndex1,
                                                                  int cellIndex2)
{
        precision m1 = getBoundaryMomentumDensity(cellIndex1, 1);
        precision m2 = getBoundaryMomentumDensity(cellIndex2, 0);

        precision d1 = getBoundaryMassDensity(cellIndex1, 1);
        precision d2 = getBoundaryMassDensity(cellIndex2, 0);

        precision v1 = m1 / d1;
        precision v2 = m2 / d2;

        precision e1 = getBoundaryEnergyDensity(cellIndex1, 1);
        precision e2 = getBoundaryEnergyDensity(cellIndex2, 0);

        precision p1 = gamma1 * (e1 - 0.5 * m1 * v1);
        precision p2 = gamma1 * (e2 - 0.5 * m2 * v2);

        precision l = (getCellLength(cellIndex1) + getCellLength(cellIndex2)) / 2.0;

        numericalFluxes[cellIndex1 + 1][0] = 0.5 * (m1 + m2) - 0.5 * l /dt * (d2 - d1);

        numericalFluxes[cellIndex1 + 1][1] = 0.5 * (v1 * m1 + p1 + v2 * m2 + p2)
                                             - 0.5 * l / dt * (m2 - m1);

        numericalFluxes[cellIndex1 + 1][2] = 0.5 * (v1 * (e1 + p1) + v2 * (e2 + p2))
                                             - 0.5 * l / dt * (e2 - e1);
}

template <typename precision, int degree>
void grid<precision, degree>::calculateNumericalFluxVanLeerFvmOpt(int cellIndex1,
                                                                  int cellIndex2)
{
        std::array<precision, 3> numericalFlux1;
        std::array<precision, 3> numericalFlux2;

        precision M1 = getMachNumber(cellIndex1, 0.0);
        precision c1 = getSoundSpeed(cellIndex1, 0.0);
        precision d1 = getMassDensity(cellIndex1, 0.0);

        precision MM1 = M1 + 1.0;
        precision MM2 = M1 - 1.0;

        precision preFactor1 = d1 * c1 / 4.0 * MM1 * MM1;
        precision preFactor2 = -d1 * c1 / 4.0 * MM2 * MM2;

        precision g1 = 1.0 + gamma1 / 2.0 * M1;
        numericalFlux1[0] = preFactor1;
        numericalFlux1[1] = preFactor1 * 2.0 * c1 / gamma * g1;
        numericalFlux1[2] = preFactor1 * 2.0 * c1 * c1 / gamma2 * g1 * g1;

        precision g2 = -1.0 + gamma1 / 2.0 * M1;
        numericalFlux2[0] = preFactor2;
        numericalFlux2[1] = preFactor2 * 2.0 * c1 / gamma * g2;
        numericalFlux2[2] = preFactor2 * 2.0 * c1 * c1 / gamma2 * g2 * g2;

        numericalFluxes[cellIndex1][0] += numericalFlux2[0];
        numericalFluxes[cellIndex1][1] += numericalFlux2[1];
        numericalFluxes[cellIndex1][2] += numericalFlux2[2];

        numericalFluxes[cellIndex1 + 1][0] = numericalFlux1[0];
        numericalFluxes[cellIndex1 + 1][1] = numericalFlux1[1];
        numericalFluxes[cellIndex1 + 1][2] = numericalFlux1[2];
}

template <typename precision, int degree>
void grid<precision, degree>::calculateNumericalFluxVanLeer(int cellIndex1,
                                                            int cellIndex2)
{
        std::array<precision, 3> numericalFlux1;
        std::array<precision, 3> numericalFlux2;

        precision d1 = getBoundaryMassDensity(cellIndex1, 1);
        precision m1 = getBoundaryMomentumDensity(cellIndex1, 1);
        precision e1 = getBoundaryEnergyDensity(cellIndex1, 1);

        precision p1 = calculatePressure(d1, m1, e1);
        precision v1 = m1 / d1;
        precision c1 = calculateSoundSpeed(d1, p1);
        precision M1 = v1 / c1;

        precision d2 = getBoundaryMassDensity(cellIndex2, 0);
        precision m2 = getBoundaryMomentumDensity(cellIndex2, 0);
        precision e2 = getBoundaryEnergyDensity(cellIndex2, 0);

        precision p2 = calculatePressure(d2, m2, e2);
        precision v2 = m2 / d2;
        precision c2 = calculateSoundSpeed(d2, p2);
        precision M2 = v2 / c2;

        precision MM1 = M1 + 1.0;
        precision MM2 = M2 - 1.0;

        precision preFactor1 = d1 * c1 / 4.0 * MM1 * MM1;
        precision preFactor2 = -d2 * c2 / 4.0 * MM2 * MM2;

        precision g1 = 1.0 + gamma1 / 2.0 * M1;
        numericalFlux1[0] = preFactor1;
        numericalFlux1[1] = preFactor1 * 2.0 * c1 / gamma * g1;
        numericalFlux1[2] = preFactor1 * 2.0 * c1 * c1 / gamma2 * g1 * g1;

        precision g2 = -1.0 + gamma1 / 2.0 * M2;
        numericalFlux2[0] = preFactor2;
        numericalFlux2[1] = preFactor2 * 2.0 * c2 / gamma * g2;
        numericalFlux2[2] = preFactor2 * 2.0 * c2 * c2 / gamma2 * g2 * g2;

        numericalFluxes[cellIndex1 + 1][0] = numericalFlux1[0] + numericalFlux2[0];
        numericalFluxes[cellIndex1 + 1][1] = numericalFlux1[1] + numericalFlux2[1];
        numericalFluxes[cellIndex1 + 1][2] = numericalFlux1[2] + numericalFlux2[2];

}

template <typename precision, int degree>
void grid<precision, degree>::addCell(precision _length, precision _position,
                                      precision _massDensityWeights[],
                                      precision _momentumDensityWeights[],
                                      precision _energyDensityWeights[],
                                      int _boundary)
{
        cell<precision, degree> newCell(_length, _position, _massDensityWeights,
                                        _momentumDensityWeights,
                                        _energyDensityWeights, _boundary, dt,
                                        gamma);

        cells.push_back(newCell);

        std::array<precision, 3> numericalFlux;
        numericalFluxes.push_back(numericalFlux);
}

template <typename precision, int degree>
int grid<precision, degree>::getDegreePoly()
{
        return degreePoly;
}

template <typename precision, int degree>
precision grid<precision, degree>::getLength()
{
        return length;
}

template <typename precision, int degree>
precision grid<precision, degree>::getCellLength(int cellIndex)
{
        return cells[cellIndex].getLength();
}

template <typename precision, int degree>
precision grid<precision, degree>::getMassDensity(int cellIndex, precision z)
{
        precision result = cells[cellIndex].getMassDensityWeight(0);

        for (int i = 0; i != cells[cellIndex].getDegreePoly(); i++)
        {
                result += cells[cellIndex].getMassDensityWeight(i + 1)
                          * lp.legendrePoly(i, z);
        }
        return result;
}

template <typename precision, int degree>
void grid<precision, degree>::getMassDensityWeights(int cellIndex,
                                                    precision massDensityWeights[])
{
        cells[cellIndex].getMassDensityWeights(massDensityWeights);
}

template <typename precision, int degree>
void grid<precision, degree>::getMomentumDensityWeights(int cellIndex,
                                                        precision momentumDensityWeights[])
{
        cells[cellIndex].getMomentumDensityWeights(momentumDensityWeights);
}

template <typename precision, int degree>
void grid<precision, degree>::getEnergyDensityWeights(int cellIndex,
                                                      precision energyDensityWeights[])
{
        cells[cellIndex].getEnergyDensityWeights(energyDensityWeights);
}

template <typename precision, int degree>
precision grid<precision, degree>::getVelocity(int cellIndex, precision z)
{
        return getMomentumDensity(cellIndex, z) / getMassDensity(cellIndex, z);
}

template <typename precision, int degree>
precision grid<precision, degree>::getMomentumDensity(int cellIndex, precision z)
{
        precision result = cells[cellIndex].getMomentumDensityWeight(0);

        for (int i = 0; i != cells[cellIndex].getDegreePoly(); i++)
        {
                result += cells[cellIndex].getMomentumDensityWeight(i + 1)
                          * lp.legendrePoly(i, z);
        }
        return result;
}

template <typename precision, int degree>
precision grid<precision, degree>::getPressure(int cellIndex, precision z)
{
        precision m = getMomentumDensity(cellIndex, z);

        return gamma1 * (getEnergyDensity(cellIndex, z) - 0.5 * m * m
                         / getMassDensity(cellIndex, z));
}

template <typename precision, int degree>
precision grid<precision, degree>::calculatePressure(precision massDensity,
                                                     precision momentumDensity,
                                                     precision energyDensity)
{
        return gamma1 * (energyDensity - 0.5 * momentumDensity * momentumDensity
                         / massDensity);
}

template <typename precision, int degree>
precision grid<precision, degree>::getEnergyDensity(int cellIndex, precision z)
{
        precision result = cells[cellIndex].getEnergyDensityWeight(0);

        for (int i = 0; i != cells[cellIndex].getDegreePoly(); i++)
        {
                result += cells[cellIndex].getEnergyDensityWeight(i + 1)
                          * lp.legendrePoly(i, z);
        }
        return result;
}

template <typename precision, int degree>
precision grid<precision, degree>::getSoundSpeed(int cellIndex, precision z)
{
        return sqrt(gamma * getPressure(cellIndex, z) / getMassDensity(cellIndex, z));
}

template <typename precision, int degree>
precision grid<precision, degree>::getMachNumber(int cellIndex, precision z)
{
        return getVelocity(cellIndex,z) / getSoundSpeed(cellIndex, z);
}

template <typename precision, int degree>
precision grid<precision, degree>::calculateSoundSpeed(precision massDensity,
                                                       precision pressure)
{
        return sqrt(gamma * pressure / massDensity);
}

template <typename precision, int degree>
precision grid<precision, degree>::getBoundaryMassDensity(int cellIndex, int b){
        precision result = cells[cellIndex].getMassDensityWeight(0);
        for (int i = 0; i != cells[cellIndex].getDegreePoly(); i++) {
                result += lp.getLegendreEvaluations(i, b)
                          * cells[cellIndex].getMassDensityWeight(i + 1);
        }
        return result;
}

template <typename precision, int degree>
precision grid<precision, degree>::getBoundaryMomentumDensity(int cellIndex, int b){
        precision result = cells[cellIndex].getMomentumDensityWeight(0);
        for (int i = 0; i != cells[cellIndex].getDegreePoly(); i++) {
                result += lp.getLegendreEvaluations(i, b)
                          * cells[cellIndex].getMomentumDensityWeight(i + 1);
        }
        return result;
}

template <typename precision, int degree>
precision grid<precision, degree>::getBoundaryEnergyDensity(int cellIndex, int b){
        precision result = cells[cellIndex].getEnergyDensityWeight(0);
        for (int i = 0; i != cells[cellIndex].getDegreePoly(); i++) {
                result += lp.getLegendreEvaluations(i, b)
                          * cells[cellIndex].getEnergyDensityWeight(i + 1);
        }
        return result;
}

template <typename precision, int degree>
precision grid<precision, degree>::evaluateMassDensity(int cellIndex, int i){
        precision result = cells[cellIndex].getMassDensityWeight(0);
        for(int k = 0; k != degreePoly; k++) {
                result += cells[cellIndex].getMassDensityWeight(k + 1)
                          * lp.getLegendreEvaluations(k, i + 2);
        }
        return result;
}

template <typename precision, int degree>
precision grid<precision, degree>::evaluateMomentumDensity(int cellIndex, int i){
        precision result = cells[cellIndex].getMomentumDensityWeight(0);
        for(int k = 0; k != degreePoly; k++) {
                result += cells[cellIndex].getMomentumDensityWeight(k + 1)
                          * lp.getLegendreEvaluations(k, i + 2);
        }
        return result;
}

template <typename precision, int degree>
precision grid<precision, degree>::evaluateEnergyDensity(int cellIndex, int i){
        precision result = cells[cellIndex].getEnergyDensityWeight(0);
        for(int k = 0; k != degreePoly; k++) {
                result += cells[cellIndex].getEnergyDensityWeight(k + 1)
                          * lp.getLegendreEvaluations(k, i + 2);
        }
        return result;
}

template <typename precision, int degree>
void grid<precision, degree>::calculateDerivativeWeights(int cellIndex, int n){
        precision d, m, e, v, eKin;
        precision l = cells[cellIndex].getLength();
        precision massFlux[numberGaussLegendrePoints];
        precision momentumFlux[numberGaussLegendrePoints];
        precision energyFlux[numberGaussLegendrePoints];
        precision newDerivativeWeights[3 * (degreePoly + 1)];

        if (degreePoly > 0) {

                for (int i = 0; i != numberGaussLegendrePoints; i++) {
                        d = evaluateMassDensity(cellIndex, i);
                        m = evaluateMomentumDensity(cellIndex, i);
                        e = evaluateEnergyDensity(cellIndex, i);

                        v =  m / d;
                        eKin = v * m / 2.0;

                        massFlux[i] = m * gaussLegendreWeights[i];
                        momentumFlux[i] = (gamma1 * e - gamma3 * eKin)
                                          * gaussLegendreWeights[i];
                        energyFlux[i] = (gamma * e - gamma1 * eKin) * v
                                        * gaussLegendreWeights[i];
                }

        }

        for (int j = 1; j != degreePoly + 1; j++) {

                newDerivativeWeights[j] = numericalFluxes[cellIndex][0]
                                          * lp.getLegendreEvaluations(j - 1, 0)
                                          - numericalFluxes[cellIndex + 1][0]
                                          * lp.getLegendreEvaluations(j - 1, 1);
                newDerivativeWeights[j + degreePoly + 1] = numericalFluxes[cellIndex][1]
                                                           * lp.getLegendreEvaluations(j - 1, 0)
                                                           - numericalFluxes[cellIndex + 1][1]
                                                           * lp.getLegendreEvaluations(j - 1, 1);
                newDerivativeWeights[j + 2 * (degreePoly + 1)] = numericalFluxes[cellIndex][2]
                                                                 * lp.getLegendreEvaluations(j - 1, 0)
                                                                 - numericalFluxes[cellIndex + 1][2]
                                                                 * lp.getLegendreEvaluations(j - 1, 1);

                for (int i = 0; i != numberGaussLegendrePoints; i++) {

                        newDerivativeWeights[j] += lp.getDerivativeLegendreEvaluations(j - 1, i)
                                                   * massFlux[i];
                        newDerivativeWeights[j + degreePoly + 1] += lp.getDerivativeLegendreEvaluations(j - 1, i)
                                                                    * momentumFlux[i];
                        newDerivativeWeights[j + 2 * (degreePoly + 1)] += lp.getDerivativeLegendreEvaluations(j - 1, i)
                                                                          * energyFlux[i];

                }


                newDerivativeWeights[j] /= l;
                newDerivativeWeights[j + degreePoly + 1] /= l;
                newDerivativeWeights[j + 2 * (degreePoly + 1)] /= l;

        }


        newDerivativeWeights[0] = numericalFluxes[cellIndex][0]
                                  - numericalFluxes[cellIndex + 1][0];
        newDerivativeWeights[degreePoly + 1] = numericalFluxes[cellIndex][1]
                                               - numericalFluxes[cellIndex + 1][1];
        newDerivativeWeights[2 * (degreePoly + 1)] = numericalFluxes[cellIndex][2]
                                                     - numericalFluxes[cellIndex + 1][2];

        newDerivativeWeights[0] /= l;
        newDerivativeWeights[degreePoly + 1] /= l;
        newDerivativeWeights[2 * (degreePoly + 1)] /= l;

        for (int j = 0; j != 3 * (degreePoly + 1); j++) {
                cells[cellIndex].updateDerivativeWeight(n, j, newDerivativeWeights[j]);
        }
}

template <typename precision, int degree>
void grid<precision, degree>::updateCellsFem()
{
        for (int j = 0; j != degreePoly + 1; j++) {

                calculateNumericalFluxVanLeer(0, 1);

                for (int i = 1; i != getNumberCells() - 1; i++)
                {
                        rungeKuttaMethod(degreePoly, j, i);
                        updateCflNumber(i);
                }

                //updateBoundaryCellLeft();
                //updateBoundaryCellRight();
        }
}

template <typename precision, int degree>
void grid<precision, degree>::updateCellsFemMpi(int numprocs, int procid)
{

        for (int j = 0; j != degreePoly + 1; j++) {
                exchangeDataMpi(numprocs, procid);
                calculateNumericalFluxVanLeer(0, 1);

                for (int i = 1; i != getNumberCells() - 1; i++)
                {
                        rungeKuttaMethod(degreePoly, j, i);
                        updateCflNumber(i);
                }

                if (procid == 0) {
                        updateBoundaryCellLeft();
                }
                if (procid == numprocs - 1 ) {
                        updateBoundaryCellRight();
                }

        }
}

template <typename precision, int degree>
void grid<precision, degree>::updateCellsFvm(int cellIndex1, int cellIndex2)
{
        calculateNumericalFluxVanLeer(cellIndex1, cellIndex1 + 1);

        for (int i = cellIndex1 + 1; i != cellIndex2 - 1; i++)
        {
                calculateNumericalFluxVanLeer(i, i + 1);
                updateCellFvm(i);
                updateCflNumber(i);
        }
        updateBoundaryCellLeft();
        updateBoundaryCellRight();
}

template <typename precision, int degree>
void grid<precision, degree>::rungeKuttaMethod(int k, int n, int cellIndex){
        return (this->*(rk[k]))(n, cellIndex);
}

template <typename precision, int degree>
void grid<precision, degree>::rungeKuttaMethod3(int n, int cellIndex){
        return (this->*(rk3[n]))(cellIndex);
}

template <typename precision, int degree>
void grid<precision, degree>::rungeKuttaMethod2(int n, int cellIndex){
        return (this->*(rk2[n]))(cellIndex);
}

template <typename precision, int degree>
void grid<precision, degree>::rungeKuttaMethod1(int n, int cellIndex){
        return (this->*(rk1[n]))(cellIndex);
}

template <typename precision, int degree>
void grid<precision, degree>::rungeKuttaMethod3Step1(int cellIndex){
        precision newWeight;
        calculateNumericalFluxVanLeer(cellIndex, cellIndex + 1);
        calculateDerivativeWeights(cellIndex, 0);

        for (int j = 0; j != 3 * (degreePoly + 1); j++) {
                newWeight = cells[cellIndex].getInitialWeight(j)
                            + dt * cells[cellIndex].getDerivativeWeight(0, j);
                cells[cellIndex].updateWeight(j, newWeight);
        }
}

template <typename precision, int degree>
void grid<precision, degree>::rungeKuttaMethod3Step2(int cellIndex){
        precision newWeight;
        calculateNumericalFluxVanLeer(cellIndex, cellIndex + 1);
        calculateDerivativeWeights(cellIndex, 1);

        for (int j = 0; j != 3 * (degreePoly + 1); j++) {
                newWeight = cells[cellIndex].getInitialWeight(j) * 3.0 / 4.0
                            + cells[cellIndex].getWeight(j) / 4.0 + dt / 4.0
                            * cells[cellIndex].getDerivativeWeight(1, j);
                cells[cellIndex].updateWeight(j, newWeight);
        }
}

template <typename precision, int degree>
void grid<precision, degree>::rungeKuttaMethod3Step3(int cellIndex){
        precision newWeight;
        calculateNumericalFluxVanLeer(cellIndex, cellIndex + 1);
        calculateDerivativeWeights(cellIndex, 2);

        for (int j = 0; j != 3 * (degreePoly + 1); j++) {
                newWeight = cells[cellIndex].getInitialWeight(j) / 3.0
                            + cells[cellIndex].getWeight(j) * 2.0 / 3.0 + dt * 2.0
                            / 3.0 * cells[cellIndex].getDerivativeWeight(2, j);
                cells[cellIndex].updateInitialWeight(j, newWeight);
                cells[cellIndex].updateWeight(j, newWeight);
        }
}

template <typename precision, int degree>
void grid<precision, degree>::rungeKuttaMethod2Step1(int cellIndex){
        precision newWeight;
        calculateNumericalFluxVanLeer(cellIndex, cellIndex + 1);
        calculateDerivativeWeights(cellIndex, 0);

        for (int j = 0; j != 3 * (degreePoly + 1); j++) {
                newWeight = cells[cellIndex].getInitialWeight(j) + dt
                            * cells[cellIndex].getDerivativeWeight(0, j);
                cells[cellIndex].updateWeight(j, newWeight);
        }
}

template <typename precision, int degree>
void grid<precision, degree>::rungeKuttaMethod2Step2(int cellIndex){
        precision newWeight;
        calculateNumericalFluxVanLeer(cellIndex, cellIndex + 1);
        calculateDerivativeWeights(cellIndex, 1);
        for (int j = 0; j != 3 * (degreePoly + 1); j++) {
                newWeight = cells[cellIndex].getInitialWeight(j) / 2.0 +
                            cells[cellIndex].getWeight(j) / 2.0 + dt / 2.0
                            * cells[cellIndex].getDerivativeWeight(1, j);
                cells[cellIndex].updateInitialWeight(j, newWeight);
                cells[cellIndex].updateWeight(j, newWeight);
        }
}

template <typename precision, int degree>
void grid<precision, degree>::rungeKuttaMethod1Step1(int cellIndex){     //Euler method
        precision newWeight;
        calculateNumericalFluxVanLeer(cellIndex, cellIndex + 1);
        calculateDerivativeWeights(cellIndex, 0);
        for (int j = 0; j != 3 * (degreePoly + 1); j++) {
                newWeight = cells[cellIndex].getWeight(j) + dt
                            * cells[cellIndex].getDerivativeWeight(0, j);
                cells[cellIndex].updateWeight(j, newWeight);
        }
}

template <typename precision, int degree>
precision grid<precision, degree>::getWeight(int cellIndex, int i){
        return cells[cellIndex].getWeight(i);
}

template <typename precision, int degree>
void grid<precision, degree>::runFem(int numberSteps){
        for (int j = 0; j != numberSteps; j++)
        {
                updateCellsFem();
        }

}

template <typename precision, int degree>
void grid<precision, degree>::exchangeDataMpi(int numprocs, int procid){
        precision bufferWeights[3 * (degreePoly + 1)];
        if (procid % 2 == 0) {
                if (procid != numprocs - 1) {
                        cells[getNumberCells() - 2].getWeights(bufferWeights);
                        MPI_Send(bufferWeights, 3 * (degreePoly + 1), MPI_DOUBLE,
                                 procid + 1, 0, MPI_COMM_WORLD);
                        MPI_Recv(bufferWeights, 3 * (degreePoly + 1), MPI_DOUBLE,
                                 procid + 1, 0, MPI_COMM_WORLD, MPI_STATUS_IGNORE);
                        cells[getNumberCells() - 1].updateWeights(bufferWeights);
                }
                if (procid != 0) {
                        cells[1].getWeights(bufferWeights);
                        MPI_Send(bufferWeights, 3 * (degreePoly + 1), MPI_DOUBLE,
                                 procid - 1, 0, MPI_COMM_WORLD);
                        MPI_Recv(bufferWeights, 3 * (degreePoly + 1), MPI_DOUBLE,
                                 procid - 1, 0, MPI_COMM_WORLD, MPI_STATUS_IGNORE);
                        cells[0].updateWeights(bufferWeights);
                }
        }else{
                MPI_Recv(bufferWeights, 3 * (degreePoly + 1), MPI_DOUBLE,
                         procid - 1, 0, MPI_COMM_WORLD, MPI_STATUS_IGNORE);
                cells[0].updateWeights(bufferWeights);
                cells[1].getWeights(bufferWeights);
                MPI_Send(bufferWeights, 3 * (degreePoly + 1), MPI_DOUBLE,
                         procid - 1, 0, MPI_COMM_WORLD);
                if (procid != numprocs - 1) {
                        MPI_Recv(bufferWeights, 3 * (degreePoly + 1), MPI_DOUBLE,
                                 procid + 1, 0, MPI_COMM_WORLD, MPI_STATUS_IGNORE);
                        cells[getNumberCells() - 1].updateWeights(bufferWeights);
                        cells[getNumberCells() - 2].getWeights(bufferWeights);
                        MPI_Send(bufferWeights, 3 * (degreePoly + 1), MPI_DOUBLE,
                                 procid + 1, 0, MPI_COMM_WORLD);
                }
        }

}

template <typename precision, int degree>
void grid<precision, degree>::runSodShockTubeUniformGridMpi(int numberCells,
                                                            int numberSteps,
                                                            int argc, char *argv[]){

        int n, remainder, d, start, end, boundary;

        int root = 0;

        int size = (numberCells + 2) * (degreePoly + 1) * 3;
        double allWeights[size];

        int *sendcounts;
        int *displs;
        int *numberCellsProc;
        precision *localWeights;


        int ierr, procid, numprocs;

        ierr = MPI_Init(&argc, &argv);
        ierr = MPI_Comm_rank(MPI_COMM_WORLD, &procid);
        ierr = MPI_Comm_size(MPI_COMM_WORLD, &numprocs);

        int localBufferSize = (ceil(numberCells / numprocs) + 2) * 3 * (degreePoly + 1);

        sendcounts = new int [numprocs];
        displs = new int [numprocs];
        numberCellsProc = new int [numprocs];
        localWeights = new precision [localBufferSize];

        precision localCellLength = length / numberCells;


        grid<double, degreePoly> localGrid(1.0, dt, 1.4);

        n = numberCells / numprocs;
        remainder = numberCells % numprocs;
        d = 0;

        for (int i = 0; i != numprocs; i++) {
                if (i < remainder) {
                        numberCellsProc[i] = n + 1;
                } else {
                        numberCellsProc[i] = n;
                }
                if (i == 0) {
                        numberCellsProc[i] += 1;
                }
                if (i == numprocs - 1) {
                        numberCellsProc[i] += 1;
                }
                sendcounts[i] = numberCellsProc[i] * (degreePoly + 1) * 3;
                displs[i] = d;
                d += sendcounts[i];
        }

        if (procid == root) {
                initSodShockTubeUniformGrid(numberCells, 1.0, 0.0, 1.0, 0.25, 0.0, 0.2);
                saveData("initSod", degreePoly + 1);
                copyAllWeights(allWeights);
                std::cout << "initilised global grid, number steps = " << numberSteps
                          << ", t = " << numberSteps * dt << ", number cells = " << numberCells
                          << ", degree poly = " << degreePoly << std::endl;
                std::cout << numprocs << " processes detected" << std::endl;
        }

        MPI_Scatterv(&allWeights, sendcounts, displs, MPI_DOUBLE, localWeights,
                     localBufferSize, MPI_DOUBLE, root, MPI_COMM_WORLD);

        precision massDensityWeights[degreePoly + 1];
        precision momentumDensityWeights[degreePoly + 1];
        precision energyDensityWeights[degreePoly + 1];

        if (procid != 0 ) {

                localGrid.addCell(localCellLength, 0.0, massDensityWeights,
                                  momentumDensityWeights, energyDensityWeights, -2);
        }

        for (int j = 0; j != numberCellsProc[procid]; j++)
        {

                for (int i = 0; i != degreePoly + 1; i++)
                {
                        massDensityWeights[i] = localWeights[i + 3 * (degreePoly + 1) * j];
                        momentumDensityWeights[i] = localWeights[i + degreePoly + 1 + 3 * (degreePoly + 1) * j];
                        energyDensityWeights[i] = localWeights[i + 2 * (degreePoly + 1) + 3 * (degreePoly + 1) * j];
                }

                if (j == 0 and procid == 0) {
                        boundary = -1;
                } else if (j == numberCellsProc[procid] - 1 and procid == numprocs - 1) {
                        boundary = 1;
                } else {
                        boundary = 0;
                }

                localGrid.addCell(localCellLength, (j + 0.5) * localCellLength,
                                  massDensityWeights, momentumDensityWeights,
                                  energyDensityWeights, boundary);

        }

        if (procid != numprocs - 1) {

                localGrid.addCell(localCellLength, length, massDensityWeights,
                                  momentumDensityWeights, energyDensityWeights, 2);

        }

        for (int j = 0; j != numberSteps; j++)
        {
                localGrid.updateCellsFemMpi(numprocs, procid);

        }

        if (procid == 0) {
                start = 0;
                end = localGrid.getNumberCells() - 1;
        }else if (procid == numprocs - 1) {
                start = 1;
                end = localGrid.getNumberCells() - 1;
        }else{
                start = 1;
                end = localGrid.getNumberCells() - 2;
        }

        for (int i = 0; i != end; i++)
        {
                for (int j = 0; j != 3 * (degreePoly + 1); j++)
                {
                        localWeights[i * 3 * (degreePoly + 1) + j] = localGrid.getWeight(i + start, j);
                }
        }

        MPI_Gatherv (localWeights, sendcounts[procid], MPI_DOUBLE, &allWeights,
                     sendcounts, displs, MPI_DOUBLE, root, MPI_COMM_WORLD);

        if (procid == root) {
                updateAllWeights(allWeights);
                saveData("endSod", degreePoly + 1);
                std::cout << "simulation finished" << std::endl;
        }

        free(sendcounts);
        free(displs);
        free(numberCellsProc);
        free(localWeights);

        ierr = MPI_Finalize();
}

template <typename precision, int degree>
void grid<precision, degree>::saveData(std::string name, int n){
        std::ofstream outputFile1;
        name += ".txt";
        outputFile1.open(name);
        int start = 1;
        int end = getNumberCells() - 1;
        precision x, dx;

        dx = 2.0 / n;

        for (int i = start; i != end; i++)
        {
                x = -1.0 + dx / 2.0;
                for (int j = 0; j != n; j++)
                {
                        outputFile1 << getMassDensity(i, x) << " ";
                        x += dx;
                }
        }
        outputFile1 << std::endl;

        for (int i = start; i != end; i++)
        {
                x = -1.0 + dx / 2.0;
                for (int j = 0; j != n; j++)
                {
                        outputFile1 << getVelocity(i, x) << " ";
                        x += dx;
                }

        }
        outputFile1 << std::endl;

        for (int i = start; i != end; i++)
        {
                x = -1.0 + dx / 2.0;
                for (int j = 0; j != n; j++)
                {
                        outputFile1 << getPressure(i, x) << " ";
                        x += dx;
                }
        }
        outputFile1 << std::endl;

        for (int i = start; i != end; i++)
        {
                x = -1.0 + dx / 2.0;
                for (int j = 0; j != n; j++)
                {
                        outputFile1 << getPressure(i, x) / getMassDensity(i, x) << " ";
                        x += dx;
                }
        }
        outputFile1 << std::endl;


        outputFile1.close();
}
