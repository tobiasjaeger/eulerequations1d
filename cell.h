/*
   Implementation of the class cell which contains the state variables mass density,
   momentum density and energy density. Each state variable is described by a set of weights.
   The solution on each cell is represented by normalised Legendre polynomials and their
   corresponding weights.
 */

template <typename precision, int degree>
class cell
{
private:
static const int degreePoly = degree;
precision dt;
precision length;
precision position;
precision weights[(degree + 1) * 3];
precision initialWeights[(degree + 1) * 3];
precision derivativeWeights[degree + 1][(degree + 1) * 3];

precision gamma;
precision gamma1;
precision gamma2;
int boundary;

public:

cell(precision _length, precision _position,
     precision _massDensityWeights[], precision _momentumDensityWeights[],
     precision _energyDensityWeights[], int _boundary,precision _dt,
     precision _gamma);

int getDegreePoly();

precision getMassDensityWeight(int i);

precision getMomentumDensityWeight(int i);

precision getEnergyDensityWeight(int i);

void getWeights(precision _weights[]);

void updateWeights(precision _weights[]);

void getMassDensityWeights(precision massDensityWeights[]);

void getMomentumDensityWeights(precision momentumDensityWeights[]);

void getEnergyDensityWeights(precision energyDensityWeights[]);

precision getLength();

precision getTimeStep();

void updateMassDensity(precision _massDensity[]);

void updateMomentumDensity(precision _momentumDensity[]);

void updateEnergyDensity(precision _energyDensity[]);

precision getWeight(int i);

void updateWeight(int i, precision newWeight);

precision getDerivativeWeight(int i, int j);

precision getInitialWeight(int i);

void updateInitialWeight(int i, precision newInitialWeight);

void updateDerivativeWeight(int i, int j, precision newDerivativeWeights);

};

template <typename precision, int degree>
int cell<precision, degree>::getDegreePoly(){
        return degreePoly;
}

template <typename precision, int degree>
cell<precision, degree>::cell(precision _length, precision _position,
                              precision _massDensityWeights[],
                              precision _momentumDensityWeights[],
                              precision _energyDensityWeights[], int _boundary,
                              precision _dt, precision _gamma)
{
        length = _length;
        position = _position;
        boundary = _boundary;
        dt = _dt;
        gamma = _gamma;
        gamma1 = _gamma - 1.0;

        for (int i = 0; i != degreePoly + 1; i++)
        {
                weights[i] = _massDensityWeights[i];
                weights[degreePoly + 1 + i] = _momentumDensityWeights[i];
                weights[2 * (degreePoly + 1) + i] = _energyDensityWeights[i];
                initialWeights[i] = _massDensityWeights[i];
                initialWeights[degreePoly + 1 + i] = _momentumDensityWeights[i];
                initialWeights[2 * (degreePoly + 1) + i] = _energyDensityWeights[i];
        }
}

template <typename precision, int degree>
precision cell<precision, degree>::getMassDensityWeight(int i)
{
        return weights[i];
}

template <typename precision, int degree>
precision cell<precision, degree>::getMomentumDensityWeight(int i)
{
        return weights[degreePoly + 1 + i];
}

template <typename precision, int degree>
precision cell<precision, degree>::getEnergyDensityWeight(int i)
{
        return weights[2 * (degreePoly + 1) + i];
}

template <typename precision, int degree>
void cell<precision, degree>::getWeights(precision _weights[])
{
        for (int i = 0; i != 3 * (degreePoly + 1); i++)
        {
                _weights[i] = weights[i];
        }
}

template <typename precision, int degree>
void cell<precision, degree>::updateWeights(precision _weights[])
{
        for (int i = 0; i != 3 * (degreePoly + 1); i++)
        {
                weights[i] = _weights[i];
        }
}

template <typename precision, int degree>
void cell<precision, degree>::getMassDensityWeights(precision massDensityWeights[])
{
        for (int i = 0; i != degreePoly + 1; i++)
        {
                massDensityWeights[i] = weights[i];
        }
}

template <typename precision, int degree>
void cell<precision, degree>::getMomentumDensityWeights(precision momentumDensityWeights[])
{
        for (int i = 0; i != degreePoly + 1; i++)
        {
                momentumDensityWeights[i] = weights[degreePoly + 1 + i];
        }
}

template <typename precision, int degree>
void cell<precision, degree>::getEnergyDensityWeights(precision energyDensityWeights[])
{
        for (int i = 0; i != degreePoly + 1; i++)
        {
                energyDensityWeights[i] = weights[2 * (degreePoly + 1) + i];
        }
}

template <typename precision, int degree>
precision cell<precision, degree>::getLength()
{
        return length;
}

template <typename precision, int degree>
precision cell<precision, degree>::getTimeStep()
{
        return dt;
}

template <typename precision, int degree>
void cell<precision, degree>::updateMassDensity(precision _massDensity[])
{
        for (int i = 0; i != degreePoly + 1; i++)
        {
                weights[i] = _massDensity[i];
        }
}

template <typename precision, int degree>
void cell<precision, degree>::updateMomentumDensity(precision _momentumDensity[])
{
        for (int i = 0; i != degreePoly + 1; i++)
        {
                weights[degreePoly + 1 + i] = _momentumDensity[i];
        }
}

template <typename precision, int degree>
void cell<precision, degree>::updateEnergyDensity(precision _energyDensity[])
{
        for (int i = 0; i != degreePoly + 1; i++)
        {
                weights[2 * (degreePoly + 1) + i] = _energyDensity[i];
        }
}

template <typename precision, int degree>
precision cell<precision, degree>::getWeight(int i){
        return weights[i];
}

template <typename precision, int degree>
void cell<precision, degree>::updateWeight(int i, precision newWeight){
        weights[i] = newWeight;
}

template <typename precision, int degree>
precision cell<precision, degree>::getDerivativeWeight(int i, int j){
        return derivativeWeights[i][j];
}

template <typename precision, int degree>
precision cell<precision, degree>::getInitialWeight(int i){
        return initialWeights[i];
}

template <typename precision, int degree>
void cell<precision, degree>::updateInitialWeight(int i, precision newInitialWeight){
        initialWeights[i] = newInitialWeight;
}

template <typename precision, int degree>
void cell<precision, degree>::updateDerivativeWeight(int i, int j, precision newDerivativeWeights){
        derivativeWeights[i][j] = newDerivativeWeights;
}
